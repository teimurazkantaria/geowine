<?php
namespace app\api;

use app\api\dto\in\LoginDto;

interface UserService
{
    public function login(LoginDto $dto);

    /**
     * Get User with related Customer (if any) and Admin (if any)
     * @param $userId
     * @return [
     *  'user' => @app\api\dto\out\UserDto,
     *  'customer' => @app\api\dto\out\CustomerDto,
     *  'admin' => Not implemented
     * ]
     */
    public function getUserDetails($userId);
}