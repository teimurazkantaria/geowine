<?php
namespace app\api\dto\out;

use app\models\Customer;

/**
 * Duplicates app\model\Customer, now there is no sensitive information, but can be added in the future,
 * so use this dto to prevent security issues.
 */
class CustomerDto
{
    public $id;
    public $user_id;
    public $first_name;
    public $last_name;
    public $balance;

    /**
     * CustomerDto constructor.
     * @param $id
     * @param $user_id
     * @param $first_name
     * @param $last_name
     * @param $balance
     */
    public function __construct($id, $user_id, $first_name, $last_name, $balance)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->balance = $balance;
    }

    public static function of(Customer $customer)
    {
        return new static($customer->id, $customer->user_id, $customer->first_name, $customer->last_name, $customer->balance);
    }
}