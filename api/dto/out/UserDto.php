<?php
namespace app\api\dto\out;

use app\models\User;

/**
 * Almost duplicates app\model\User, but hides sensitive information, such password, access token, etc.
 */
class UserDto
{
    public $id;
    public $username;
    public $email;

    /**
     * UserDto constructor.
     * @param $id
     * @param $username
     * @param $email
     */
    public function __construct($id, $username, $email)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
    }

    public static function of(User $user)
    {
        return new static($user->id, $user->username, $user->email);

    }
}
