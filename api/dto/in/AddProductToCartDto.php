<?php
namespace app\api\dto\in;

use yii\base\Model;

class AddProductToCartDto extends Model
{
    public $customerId;
    public $productId;
    public $quantity;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['customerId', 'productId', 'quantity'], 'required'],
            [['quantity'], 'number', 'min' => 1],
        ];
    }
}
