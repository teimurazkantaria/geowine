<?php
namespace app\api\internal;

use app\api\dto\in\LoginDto;
use app\api\dto\out\CustomerDto;
use app\api\dto\out\UserDto;
use app\api\UserService;
use app\core\errors\ItemNotFoundException;
use app\models\Customer;
use app\models\User;

class DefaultUserService implements UserService
{
    /**
     * @inheritdoc
     */
    public function login(LoginDto $dto)
    {
        // TODO: Implement login() method.
    }

    /**
     * @inheritdoc
     */
    public function getUserDetails($userId)
    {
        // TODO:: check permission
        $user = User::findOne($userId);

        if ($user === null) {
            throw new ItemNotFoundException("User with id {$userId} not found");
        }

        $customer = Customer::find()->where(['user_id' => $userId])->one();

        return [
            'user' => UserDto::of($user),
            'customer' => CustomerDto::of($customer),
        ];
    }
}
