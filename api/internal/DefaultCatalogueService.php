<?php
namespace app\api\internal;

use app\api\CatalogueService;
use app\models\Category;
use app\models\Product;

class DefaultCatalogueService implements CatalogueService
{
    /**
     * @inheritdoc
     */
    public function getProducts()
    {
        $products = Product::find()->all();
        return $products;
    }

    /**
     * @inheritdoc
     */
    public function getCategories()
    {
        $categories = Category::find()->all();
        return $categories;
    }
}
