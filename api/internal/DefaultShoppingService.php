<?php
namespace app\api\internal;

use app\core\errors\ItemNotFoundException;
use Yii;
use app\api\dto\in\AddProductToCartDto;
use app\api\ShoppingService;
use app\core\errors\InternalErrorException;
use app\core\errors\ValidationException;
use app\models\Customer;
use app\models\Product;
use app\models\ShoppingCart;
use app\models\ShoppingCartItem;

class DefaultShoppingService implements ShoppingService
{
    /**
     * @inheritdoc
     */
    public function addProductToCart(AddProductToCartDto $dto)
    {
        // TODO:: check if current user has permission to add product to provided customer's shopping cart

        $dto->validate();
        if ($dto->hasErrors()) {
            throw new ValidationException("Incorrect data provided", ['errors' => $dto->errors]);
        }

        $product = Product::findOne($dto->productId);
        if ($product === null) {
            throw new ValidationException("Product with id {$dto->productId} does not exist");
        }

        $customer = Customer::find()
            ->with('shoppingCart')
            ->where(['id' => $dto->customerId])
            ->one();

        if ($customer === null) {
            throw new ValidationException("Customer with id {$dto->customerId} not found");
        }

        if ($customer->shoppingCart === null) {
            // Create shopping cart for customer if not yet created
            $shoppingCart = new ShoppingCart(['customer_id' => $customer->id, 'total_sum' => 0]);
            $shoppingCart->save();
        } else {
            // Else retrieve existing
            $shoppingCart = ShoppingCart::find()->with('items')->where(['customer_id' => $customer->id])->one();
        }

        $shoppingCart->addProduct($product, $dto->quantity);

        return $shoppingCart;
    }

    /**
     * @inheritdoc
     */
    public function getShoppingCart($customerId)
    {
        // TODO:: check if current user has permission to view customer's shopping cart
        $shoppingCart = ShoppingCart::find()->where(['customer_id'=>$customerId])->one();

        return $shoppingCart;
    }

    /**
     * @inheritdoc
     */
    public function getShoppingCartItems($customerId)
    {
        // TODO:: check if current user has permission to view customer's shopping cart
        $items = ShoppingCartItem::find()->with('product')->where(['customer_id' => $customerId])->all();
        return $items;
    }
}