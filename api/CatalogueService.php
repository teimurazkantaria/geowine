<?php
namespace app\api;

interface CatalogueService
{
    /**
     * Get products
     * @return array of @app\models\Product
     */
    public function getProducts();

    /**
     * Get categories
     * @return array of @app\models\Category
     */
    public function getCategories();
}
