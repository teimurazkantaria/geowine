<?php
namespace app\api;

use app\api\dto\in\AddProductToCartDto;

interface ShoppingService
{
    /**
     * Add product with given id to customers shopping cart.
     *
     * @param AddProductToCartDto $dto
     * @return @app\models\ShoppingCart - updated shopping cart
     */
    public function addProductToCart(AddProductToCartDto $dto);

    /**
     * Get shopping cart without items
     * @param $customerId
     * @return @app\model\ShoppingCart
     */
    public function getShoppingCart($customerId);

    /**
     * Get shopping cart items
     * @param $customerId
     * @return []@app\model\ShoppingCartItem
     */
    public function getShoppingCartItems($customerId);
}