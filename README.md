# GeoWine #

DIRECTORY STRUCTURE
-------------------

      api/                Api layer is central gateway for interfaces: rest and web.
      config/             Application main configurations
      core/               Core functionality which share all layers
      interfaces/         Presentation Layer: Responsible for presenting information to the user and interpreting user commands.
        rest/             Rest presentation layer - for example, rest interface may be used for SPA or Mobile Client.
        web/              Web presentation layer - VC part of classical MVC architecture. M part is api layer
            admin/        Admin module                    
      mail/               Contains view files for e-mails
      migrations/         Database migrations
      models/             Model classes
      runtime/            Files generated during runtime
      vendor/             Dependent 3rd-party packages


REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


Notes
-----
Api layer is central gateway for interfaces: rest and web.

web interface may be used as admin backend.
So main logic resides in Api layer.

Api Endpoints
-------------
Rest endpoints actually not implemented as classical REST.
Classical rest assumes working with recources, like POST /product, PUT /product/1.
Instead, I use url-s like /shopping/add-product-to-cart, or /catalogue/products/change-product-name.
The reason of this is that I follow Task Driven User Interface.

@see http://kylecordes.com/2014/task-based-user-interfaces


### Naming convention ###
Yii uses camelCase, but maps table fields in active
records as is, e.g.: if field in mysql is underscore, than field name 
of the class is under_score as well. 
Cannot make fields of data base camelCase, since it may cause problems 
between windows/linux migration.

Front End
---------
Front end implemented as Single Page Application, with React.js and Flux as separate project.
