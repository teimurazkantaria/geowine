<?php
namespace app\core\errors;

use Exception;

/**
 * Class AppException
 * @package core\errors
 *
 * Base exception for all application exceptions
 */
abstract class AppException extends \Exception
{
    /**
     * Additional data for exception
     * @var
     */
    private $data;

    /**
     * AppException constructor.
     * @param string $message
     * @param array $data - additional data for exception
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message, $data = [], $code = null, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
