<?php

use yii\db\Migration;

class m170116_062921_changed_on_delete_to_restrict_for_order_items_for_product_id extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `order_items` 
                DROP FOREIGN KEY `order_lines_product_id_fk`;
                ALTER TABLE `order_items` 
                ADD CONSTRAINT `order_lines_product_id_fk`
                  FOREIGN KEY (`product_id`)
                  REFERENCES `products` (`id`)
                  ON DELETE RESTRICT
                  ON UPDATE CASCADE;
        ");

    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `order_items` 
                DROP FOREIGN KEY `order_lines_product_id_fk`;
                ALTER TABLE `order_items` 
                ADD CONSTRAINT `order_lines_product_id_fk`
                  FOREIGN KEY (`product_id`)
                  REFERENCES `products` (`id`)
                  ON DELETE CASCADE
                  ON UPDATE CASCADE;
        ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
