<?php

use yii\db\Migration;

class m170115_204455_create_table_orders extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `orders` (
          `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
          `customer_id` BIGINT UNSIGNED NOT NULL,
          `total_sum` DECIMAL(13,4) NOT NULL,
          `status` TINYINT NOT NULL,
          `created` DATETIME NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `orders_customer_id_fk_idx` (`customer_id` ASC),
          CONSTRAINT `orders_customer_id_fk`
            FOREIGN KEY (`customer_id`)
            REFERENCES `customers` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE);
        ");

    }

    public function down()
    {
        $this->execute("DROP TABLE `orders`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
