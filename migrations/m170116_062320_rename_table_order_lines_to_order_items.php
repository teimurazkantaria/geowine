<?php

use yii\db\Migration;

class m170116_062320_rename_table_order_lines_to_order_items extends Migration
{
    public function up()
    {
        $this->execute("
        ALTER TABLE `order_lines` 
            RENAME TO  `order_items` ;
        ");
    }

    public function down()
    {
        $this->execute("
        ALTER TABLE `order_items` 
            RENAME TO  `order_lines` ;
        ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
