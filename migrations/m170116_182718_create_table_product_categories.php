<?php

use yii\db\Migration;

class m170116_182718_create_table_product_categories extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `product_categories` (
              `product_id` BIGINT UNSIGNED NOT NULL,
              `category_id` INT UNSIGNED NOT NULL,
              PRIMARY KEY (`product_id`, `category_id`),
              INDEX `product_categories_category_fk_idx` (`category_id` ASC),
              CONSTRAINT `product_categories_product_id_fk`
                FOREIGN KEY (`product_id`)
                REFERENCES `products` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `product_categories_category_fk`
                FOREIGN KEY (`category_id`)
                REFERENCES `categories` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE);
        ");

    }

    public function down()
    {
        $this->execute("DROP TABLE `product_categories`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
