<?php

use yii\db\Migration;

class m170117_134002_renamte_table_basket_items_to_shopping_cart_items extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `basket_items` 
                RENAME TO  `shopping_cart_items` ;
        ");
    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `shopping_cart_items` 
                RENAME TO  `bakset_items` ;
        ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
