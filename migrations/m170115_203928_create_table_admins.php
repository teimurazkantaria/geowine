<?php

use yii\db\Migration;

class m170115_203928_create_table_admins extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `admins` (
              `id` BIGINT UNSIGNED NOT NULL,
              `user_id` BIGINT UNSIGNED NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `admins_user_id_pk_idx` (`user_id` ASC),
              CONSTRAINT `admins_user_id_pk`
                FOREIGN KEY (`user_id`)
                REFERENCES `users` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE);
        ");
    }

    public function down()
    {
        $this->execute('DROP TABLE `admins`');
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
