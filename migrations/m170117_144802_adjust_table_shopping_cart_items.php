<?php

use yii\db\Migration;

class m170117_144802_adjust_table_shopping_cart_items extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `shopping_cart_items` 
                DROP FOREIGN KEY `bakset_items_product_id_fk`,
                DROP FOREIGN KEY `basket_items_customer_id_fk`;
                ALTER TABLE `shopping_cart_items` 
                ADD COLUMN `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
                DROP INDEX `basket_items_product_id_fk_idx` ,
                ADD INDEX `shopping_cart_items_product_id_fk_idx` (`product_id` ASC),
                DROP PRIMARY KEY,
                ADD PRIMARY KEY (`id`),
                ADD INDEX `shopping_cart_items_customer_id_fk_idx` (`customer_id` ASC);
                ALTER TABLE `shopping_cart_items` 
                ADD CONSTRAINT `shopping_cart_items_product_id_fk`
                  FOREIGN KEY (`product_id`)
                  REFERENCES `products` (`id`)
                  ON UPDATE CASCADE,
                ADD CONSTRAINT `shopping_cart_items_customer_id_fk`
                  FOREIGN KEY (`customer_id`)
                  REFERENCES `customers` (`id`)
                  ON DELETE CASCADE
                  ON UPDATE CASCADE;
        ");

    }

    public function down()
    {
        echo "m170117_144802_adjust_table_shopping_cart_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
