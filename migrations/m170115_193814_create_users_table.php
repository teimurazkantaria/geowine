<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170115_193814_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("
            CREATE TABLE `users` (
              `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
              `username` varchar(45) NOT NULL,
              `email` varchar(255) NOT NULL,
              `password_hash` varchar(64) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `users_username_idx` (`username`) USING BTREE,
              KEY `users_email_ids` (`email`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute("DROP TABLE `users`");
    }
}
