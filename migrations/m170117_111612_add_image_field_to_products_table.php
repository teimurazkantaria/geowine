<?php

use yii\db\Migration;

class m170117_111612_add_image_field_to_products_table extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `products` 
                ADD COLUMN `image` VARCHAR(255) NULL AFTER `quantity_in_store`;
        ");

    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `products` 
                DROP COLUMN `image`;
        ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
