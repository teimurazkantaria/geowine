<?php

use yii\db\Migration;

class m170115_200218_create_table_products extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `products` (
              `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
              `title` VARCHAR(255) NOT NULL,
              `short_description` MEDIUMTEXT NOT NULL,
              `description` TEXT NULL,
              `unit` VARCHAR(10) NOT NULL,
              `price` DECIMAL(13,4) NOT NULL,
              `quantity_in_store` DECIMAL(13,4) NOT NULL,
              PRIMARY KEY (`id`));
        ");

    }

    public function down()
    {
        $this->execute("DROP TABLE `products`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
