<?php

use yii\db\Migration;

class m170116_182434_create_table_categories extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `categories` (
              `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
              `title` VARCHAR(255) NULL,
              PRIMARY KEY (`id`));
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE `categories`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
