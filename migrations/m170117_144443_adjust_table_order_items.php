<?php

use yii\db\Migration;

class m170117_144443_adjust_table_order_items extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `order_items` 
                DROP FOREIGN KEY `order_lines_order_id_fk`,
                DROP FOREIGN KEY `order_lines_product_id_fk`;
                ALTER TABLE `order_items` 
                ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT FIRST,
                DROP PRIMARY KEY,
                ADD PRIMARY KEY (`id`),
                DROP INDEX `order_lines_product_id_fk_idx` ,
                ADD INDEX `order_items_product_id_fk_idx` (`product_id` ASC),
                ADD INDEX `order_items_order_id_idx` ();
                ALTER TABLE `order_items` 
                ADD CONSTRAINT `order_items_order_id_fk`
                  FOREIGN KEY (`order_id`)
                  REFERENCES `orders` (`id`)
                  ON DELETE CASCADE
                  ON UPDATE CASCADE,
                ADD CONSTRAINT `order_items_product_id_fk`
                  FOREIGN KEY (`product_id`)
                  REFERENCES `products` (`id`)
                  ON UPDATE CASCADE;
        ");
    }

    public function down()
    {
        echo "m170117_144443_adjust_table_order_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
