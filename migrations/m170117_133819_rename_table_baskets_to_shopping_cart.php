<?php

use yii\db\Migration;

class m170117_133819_rename_table_baskets_to_shopping_cart extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `baskets` 
                RENAME TO  `shopping_carts` ;
        ");

    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `shopping_carts` 
                RENAME TO  `baskets` ;
        ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
