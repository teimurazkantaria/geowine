<?php

use yii\db\Migration;

class m170116_061929_create_table_baskets extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `baskets` (
              `customer_id` BIGINT UNSIGNED NOT NULL,
              `total_sum` DECIMAL(13,4) NOT NULL,
              PRIMARY KEY (`customer_id`),
              CONSTRAINT `baskets_customer_id_fk`
                FOREIGN KEY (`customer_id`)
                REFERENCES `customers` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE);
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE `baksets`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
