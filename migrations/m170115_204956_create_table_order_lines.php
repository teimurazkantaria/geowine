<?php

use yii\db\Migration;

class m170115_204956_create_table_order_lines extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `order_lines` (
              `order_id` BIGINT UNSIGNED NOT NULL,
              `product_id` BIGINT UNSIGNED NOT NULL,
              `quantity` DECIMAL(13,4) NOT NULL,
              `price` DECIMAL(13,4) NOT NULL,
              `sum` DECIMAL(13,4) NOT NULL,
              PRIMARY KEY (`order_id`, `product_id`),
              INDEX `order_lines_product_id_fk_idx` (`product_id` ASC),
              CONSTRAINT `order_lines_order_id_fk`
                FOREIGN KEY (`order_id`)
                REFERENCES `orders` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `order_lines_product_id_fk`
                FOREIGN KEY (`product_id`)
                REFERENCES `products` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE);
        ");

    }

    public function down()
    {
        $this->execute('DROP TABLE `order_lines`');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
