<?php

use yii\db\Migration;

class m170116_062728_create_table_basket_items extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `basket_items` (
              `customer_id` bigint(20) unsigned NOT NULL,
              `product_id` bigint(20) unsigned NOT NULL,
              `quantity` decimal(13,4) NOT NULL,
              `price` decimal(13,4) NOT NULL,
              `sum` decimal(13,4) NOT NULL,
              PRIMARY KEY (`customer_id`,`product_id`),
              KEY `basket_items_product_id_fk_idx` (`product_id`),
              CONSTRAINT `basket_items_customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `bakset_items_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

    }

    public function down()
    {
        $this->execute("DROP TABLE `basket_items`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
