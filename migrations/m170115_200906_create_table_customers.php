<?php

use yii\db\Migration;

class m170115_200906_create_table_customers extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `customers` (
          `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
          `user_id` BIGINT UNSIGNED NOT NULL,
          `first_name` VARCHAR(255) NOT NULL,
          `last_name` VARCHAR(255) NULL,
          `balance` DECIMAL(13,4) NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `customers_user_id_fk_idx` (`user_id` ASC),
          CONSTRAINT `customers_user_id_fk`
            FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE);

        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE `customers`");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
