<?php
namespace app\interfaces\web\admin;

use Yii;

/**
 * Admin Module
 *
 * Each module should have a unique module class which extends from yii\base\Module.
 * The class should be located directly under the module's base path.
 */
class Module extends \yii\base\Module
{
}
