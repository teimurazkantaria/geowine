<?php

$config = [
    'modules' => [
        'admin' => [
            'class' => 'app\interfaces\web\admin\Module',
        ],
    ],
    'bootstrap' => ['admin'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '7jDmg6tdfPoUykiNEcRij0rXFG9-7gCL',
        ],
        'errorHandler' => [
            'errorAction' => 'admin/site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment - enable crud generator in dev.
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;