<?php
namespace app\interfaces\rest;

use app\models\User;
use Yii;

/**
 * Rest Module
 *
 * Each module should have a unique module class which extends from yii\base\Module.
 * The class should be located directly under the module's base path.
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        // Register error handler for rest api
        $handler = new ExceptionHandler;
        Yii::$app->set('errorHandler', $handler);
        $handler->register();

        // Fore user login, just for demo. No time for login / sign up implementation.
        $identity = User::findByUsername("teimuraz");
        Yii::$app->user->login($identity);
    }
}
