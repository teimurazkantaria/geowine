<?php

$config = [
    'modules' => [
        'rest' => [
            'class' => 'app\interfaces\rest\Module',
        ],
    ],
    'bootstrap' => ['rest'],
    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'GET catalogue/products' => 'rest/catalogue/products',
                'GET catalogue/categories' => 'rest/catalogue/categories',
                'POST shopping/add-product-to-cart' => 'rest/shopping/add-product-to-cart',
                'OPTIONS shopping/add-product-to-cart' => 'rest/common/options',
                'GET common/init' => 'rest/common/init',
                'GET users/me' => 'rest/users/me',
                'GET shopping/shopping-cart/<customerId:\d+>' => 'rest/shopping/shopping-cart',
                'GET shopping/shopping-cart/<customerId:\d+>/items' => 'rest/shopping/shopping-cart-items',
            ],
        ],
    ],
];

return $config;
