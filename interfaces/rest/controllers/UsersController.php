<?php
namespace app\interfaces\rest\controllers;

use Yii;
use yii\base\Module;
use app\api\UserService;

class UsersController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * CatalogueController constructor.
     * @param string $id
     * @param Module $module
     * @param UserService $userService
     * @param array $config
     */
    public function __construct($id, Module $module, UserService $userService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->userService = $userService;
    }

    /**
     * Get currently logged in user details
     *
     * Responds with [
     *  'user' => @app\models\UserDto,
     *  'customer' => []@app\models\CustomerDto,
     * ]
     * Json serialized
     *
     */
    public function actionMe()
    {
        $userId = Yii::$app->user->id;
        $result = $this->userService->getUserDetails($userId);
        return $result;
    }
}
