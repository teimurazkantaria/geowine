<?php
namespace app\interfaces\rest\controllers;

use app\api\CatalogueService;
use app\api\ShoppingService;
use app\api\UserService;
use Yii;
use yii\base\Module;
use yii\rest\Controller;

class CommonController extends Controller
{
    /**
     * @var CatalogueService
     */
    private $catalogueService;

    /**
     * @var ShoppingService
     */
    private $shoppingService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * CommonController constructor.
     * @param string $id
     * @param Module $module
     * @param CatalogueService $catalogueService
     * @param UserService $userService
     * @param ShoppingService $shoppingService - injected by Yii DI container
     * @param array $config
     */
    public function __construct($id,
        Module $module,
        CatalogueService $catalogueService,
        UserService $userService,
        ShoppingService $shoppingService,
        array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->catalogueService = $catalogueService;
        $this->userService = $userService;
        $this->shoppingService = $shoppingService;
    }

    /**
     * Get initial data for the client
     * @return [
     *   'userDetails' => see @app\api\UserService::getUserDetails documentation
     *   'shoppingCart' => @app\models\ShoppingCart,
     *   'categories' => []@app\models\Category
     * ]
     *
     */
    public function actionInit()
    {
        // Hard code customer id here
        // TODO:: retrieve customer by currently logged in user
        $userDetails = $this->userService->getUserDetails(Yii::$app->user->id);
        return [
            'userDetails' => $userDetails,
            'shoppingCart' => $this->shoppingService->getShoppingCart($userDetails['customer']->id),
            'categories' => $this->catalogueService->getCategories(),
        ];
    }

    /**
     * Companion action for post, put, etc, requests, to indicate that those actions are supported
     */
    public function actionOptions()
    {

    }
}
