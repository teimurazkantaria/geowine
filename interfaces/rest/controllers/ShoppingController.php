<?php
namespace app\interfaces\rest\controllers;

use Yii;
use yii\base\Module;
use app\api\ShoppingService;
use app\api\dto\in\AddProductToCartDto;

/**
 * Shopping rest controller
 */
class ShoppingController extends Controller
{
    /**
     * @var ShoppingService
     */
    private $shoppingService;

    /**
     * ShoppingController constructor.
     * @param string $id
     * @param Module $module
     * @param ShoppingService $shoppingService - injected by Yii DI container
     * @param array $config
     */
    public function __construct($id, Module $module, ShoppingService $shoppingService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->shoppingService = $shoppingService;
    }

    /**
     * Add product to shopping cart
     *
     * Responds with [
     *  'shoppingCart' => @app\models\ShoppingCart,
     *  'items' => []@app\models\ShoppingCartItem
     * ]
     * Json serialized
     */
    public function actionAddProductToCart()
    {
        $request = Yii::$app->getRequest();
        $dto = new AddProductToCartDto([
            'customerId' => $request->getBodyParam('customerId'),
            'productId' => $request->getBodyParam('productId'),
            'quantity' => $request->getBodyParam('quantity')
        ]);
        $shoppingCart = $this->shoppingService->addProductToCart($dto);
        return ['shoppingCart' => $shoppingCart, 'items' => $shoppingCart->items];
    }

    /**
     * Get shopping cart without items
     * @param $customerId
     * @return @app\models\ShoppingCart
     * Json serialized
     */
    public function actionShoppingCart($customerId)
    {
        return $this->shoppingService->getShoppingCart($customerId);
    }

    /**
     * Get shopping cart items with products
     * @param $customerId
     * @return [][
     *   'itemInfo' => @app\models\ShoppingCartItem,
     *   'product' => @app\models\Product
     * ]
     * Json serialized
     */
    public function actionShoppingCartItems($customerId)
    {
        // Format data properly, Yii does not serialize nested relations, so do it explicitly
        $items = $this->shoppingService->getShoppingCartItems($customerId);
        $itemsFormatted = [];

        foreach ($items as $item) {
            $productWithoutFilLDescription = $item->product;
            $productWithoutFilLDescription->description = null;
            $itemFormatted = [
                'itemInfo' => $item,
                'product' => $productWithoutFilLDescription,
            ];

            $itemsFormatted[] = $itemFormatted;
        }

        return $itemsFormatted;
    }
}
