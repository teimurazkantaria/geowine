<?php
namespace app\interfaces\rest\controllers;

use Yii;
use yii\base\Module;
use app\api\CatalogueService;
use yii\web\Response;

/**
 * Catalogue rest controller
 */
class CatalogueController extends Controller
{
    /**
     * @var CatalogueService
     */
    private $catalogueService;

    /**
     * CatalogueController constructor.
     * @param string $id
     * @param Module $module
     * @param CatalogueService $catalogueService - injected by Yii DI container
     * @param array $config
     */
    public function __construct($id, Module $module, CatalogueService $catalogueService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->catalogueService = $catalogueService;
    }

    /**
     * Get products by provided category
     * @return []@app\models\Product without full description
     * Json serialized
     */
    public function actionProducts()
    {
        $products = $this->catalogueService->getProducts();
        // Exclude full description from products, to prevent bandwidth overhead.
        // Full description needed only for individual product detailed view.
        $productsWithoutFullDescription = [];
        foreach ($products as $product) {
            $product->description = null;
            $productsWithoutFullDescription[] = $product;
        }
        return  $productsWithoutFullDescription;
    }

    /**
     * Get categories
     * @return []@app\models\Category
     * Json serialized
     */
    public function actionCategories()
    {
        return $this->catalogueService->getCategories();
    }
}
