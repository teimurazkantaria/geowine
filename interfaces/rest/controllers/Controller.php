<?php
namespace app\interfaces\rest\controllers;

use Yii;
use yii\web\Response;

/**
 * Base controller for other rest controllers
 */
abstract class Controller extends \yii\rest\Controller
{
    public function behaviors()
    {
        // Forse json response
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formatParam' => '_format',
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                    'application/xml' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ]);
    }
}
