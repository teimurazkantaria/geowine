<?php
namespace app\interfaces\rest;

use app\core\errors\AppException;
use Yii;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\core\errors\ForbiddenException;
use app\core\errors\ItemNotFoundException;
use app\core\errors\ValidationException;

/**
 * Class ExceptionHandler
 *
 * Handles exception thrown from app, converts to appropriate http status codes, adds unique id for each exception
 * so it will be easier to debug api user issues.
 */
class ExceptionHandler extends \yii\web\ErrorHandler
{
    /**
     * Unique exception id.
     */
    private $exceptionId;

    /**
     * @inheridoc
     * @param \Exception $exception
     * Note: overridden, adds exception id to the response, hides stacktrace for security reason
     */
    protected function renderException($exception)
    {
        // data to send to consumer of api
        $responseData = [
            'id'=>null,
            'message'=>$exception->getMessage(),
            'additionalData'=>[],
        ];
        $statusCode = null;
        switch (true) {
            case $exception instanceof ForbiddenException:
                $statusCode = 403;
                break;
            case $exception instanceof ItemNotFoundException:
            case $exception instanceof NotFoundHttpException:
                $statusCode = 404;
                break;
            case $exception instanceof ValidationException:
                $statusCode = 400;
                break;
            default:
                $statusCode = 500;
                // Do not show internal error message to end user for security reason, actual message and stack trace
                // can be view in the log by error id
                $responseData['message'] = 'Internal Server Error';
        }

        $responseData['id'] = $this->exceptionId;
        if ($exception instanceof AppException) {
            $responseData['additionalData'] = $exception->getData();
        }

        if (Yii::$app->has('response')) {
            $response = Yii::$app->getResponse();
        } else {
            $response = new Response();
        }

        // if in development mode, show exception
        if (defined('YII_ENV') && YII_ENV == 'dev')
            $responseData['exception'] = $this->serializeException($exception);

        $response->data = Json::encode($responseData);
        $response->setStatusCode($statusCode);
        $response->send();
    }

    private function serializeException($exception)
    {
        $data = [
            'message' => $exception->getMessage(),
            'trace' => $exception->getTraceAsString()
        ];
        return $data;
    }

    /**
     * @inheritdoc
     * Log exception. This method is called before 'renderException', so exception id is generated here, logged and sent
     * by 'renderException' method to the api consumer
     */
    public function logException($exception)
    {
        $this->exceptionId = $this->generateExceptionId();
        $category = get_class($exception);
        if ($exception instanceof HttpException) {
            $category = 'yii\\web\\HttpException:' . $exception->statusCode;
        } elseif ($exception instanceof \ErrorException) {
            $category .= ':' . $exception->getSeverity();
        }
        Yii::error('['.$this->exceptionId.'] '.$exception, $category);
    }

    /**
     * Generate unique exception id
     * @return string
     */
    private function generateExceptionId()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}
