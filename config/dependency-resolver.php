<?php

use Yii;

// dependency injection mapping
Yii::$container->set('app\api\CatalogueService', 'app\api\internal\DefaultCatalogueService');
Yii::$container->set('app\api\ShoppingService', 'app\api\internal\DefaultShoppingService');
Yii::$container->set('app\api\UserService', 'app\api\internal\DefaultUserService');
