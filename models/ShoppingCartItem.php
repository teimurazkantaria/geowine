<?php

namespace app\models;

use Yii;

/**
 * @property string $id
 * @property string $customer_id
 * @property string $product_id
 * @property string $quantity
 * @property string $price
 * @property string $sum
 *
 * @property Product $product
 * @property Customer $customer
 */
class ShoppingCartItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopping_cart_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'product_id', 'quantity', 'price', 'sum'], 'required'],
            [['id', 'customer_id', 'product_id'], 'integer'],
            [['quantity', 'price', 'sum'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'sum' => 'Sum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * Recalculate order line sum
     */
    public function recalculateSum()
    {
        $this->sum = $this->quantity * $this->price;
    }
}
