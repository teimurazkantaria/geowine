<?php

namespace app\models;

use app\core\errors\InternalErrorException;
use Yii;

/**
 * @property string $customer_id
 * @property string $total_sum
 *
 * @property Customer $customer
 * @property ShoppingCartItem[] $items
 */
class ShoppingCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopping_carts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'total_sum'], 'required'],
            [['customer_id'], 'integer'],
            [['total_sum'], 'number'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'total_sum' => 'Total Sum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(ShoppingCartItem::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Add product to shopping cart
     * @param $product - app\models\Product
     * @param $quantity
     * @throws InternalErrorException
     */
    public function addProduct($product, $quantity)
    {
        // If provided product with same price already existing in card, adjust quantity of item
        $items = $this->items;
        $productFound = false;
        $cartItem = null;
        foreach ($items as $item) {
            if ($item->product_id == $product->id && $item->price == $product->price)  {
                $item->quantity += $quantity;
                $cartItem = $item;
                $productFound = true;
                break;
            }
        }

        // Else, add new item if new product or same product but with different price
        if (!$productFound) {
            $item = new ShoppingCartItem([
                'customer_id' => $this->customer_id,
                'product_id' => $product->id,
                'quantity' => $quantity,
                'price' => $product->price,
            ]);
            $cartItem = $item;
        }

        // Should not happen, but check for any case
        if ($cartItem === null) {
            throw new InternalErrorException('Non-empty cart item expected');
        }

        $cartItem->recalculateSum();

        $transaction = Yii::$app->db->beginTransaction();
        if (!$cartItem->save()) {
            $transaction->rollBack();
        }
        // TODO:: optimize, remove extra db round trip
        if (!$this->refresh()) {
            $transaction->rollBack();
        }
        $this->recalculateTotalSum();

        if (!$this->save()) {
            $transaction->rollBack();
        }
        $transaction->commit();
    }

    /**
     * Recalculate total sum of items
     */
    private function recalculateTotalSum()
    {
        $sum = 0;
        foreach ($this->items as $item) {
            $sum += $item->sum;
        }
        $this->total_sum = $sum;
    }
}
