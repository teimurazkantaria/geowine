<?php

namespace app\models;

use Yii;

/**
 * Order represents history of customer's purchased items.
 *
 * @property string $id
 * @property string $customer_id
 * @property string $total_sum
 * @property integer $status
 * @property string $created
 *
 * @property OrderItem[] $orderItems
 * @property Product[] $products
 * @property Customer $customer
 */
class Order extends \yii\db\ActiveRecord
{

    /**
     * Use just two statuses for demo.
     */

    const STATUS_COMPLETED = 1;

    const STATUS_CANCELED = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'total_sum', 'status', 'created'], 'required'],
            [['customer_id', 'status'], 'integer'],
            [['total_sum'], 'number'],
            [['created'], 'safe'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'total_sum' => 'Total Sum',
            'status' => 'Status',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('order_lines', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
