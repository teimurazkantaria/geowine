<?php

namespace app\models;

/**
 * User - a person or application, which interacts with the system.
 *
 * Entities such Customer, Admin are linked to the User.
 * Customer of the application may be at the same time an Admin of this application.
 * So User is entity which identifies WHO is person or application which interacts with the system - e.g. authentication.
 * Customers, Admins, etc - identify WHAT this user can do, so basically - eg. authorization.
 * (Customers and Admin may perform more granular authorization check based on some params and data).
 *
 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'email', 'password_hash'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO:: implement
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return User
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        // TODO:: implement
        return null;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        // TODO:: implement
        return null;
    }
}
