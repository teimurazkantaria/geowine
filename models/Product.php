<?php

namespace app\models;

use Yii;

/**
 * @property string $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $unit
 * @property string $price
 * @property string $quantity_in_store
 * @property Category[] $categories
 * @property string $image
 *
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_description', 'unit', 'price', 'quantity_in_store'], 'required'],
            [['short_description', 'description', 'image'], 'string'],
            [['price', 'quantity_in_store'], 'number'],
            [['title', 'image'], 'string', 'max' => 255],
            [['unit'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'unit' => 'Unit',
            'price' => 'Price',
            'quantity_in_store' => 'Quantity In Store',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('product_categories', ['product_id' => 'id']);
    }
}
